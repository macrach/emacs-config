(require 'org)
(org-babel-load-file (expand-file-name "config.org" user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(doom-themes doom-modeline tramp-term eshell-prompt-extras exec-path-from-shell expand-region all-the-icons treemacs-magit treemacs-projectile treemacs counsel-projectile avy counsel swiper smartparens smooth-scrolling magit org-bullets async projectile which-key rustic lsp-ui lsp-mode company terraform-doc terraform-mode ansible-doc ansible plantuml-mode web-mode groovy-mode jsonnet-mode flycheck-yamllint yaml-mode json-mode toml-mode dockerfile-mode markdown-mode diminish use-package))
 '(warning-suppress-log-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
